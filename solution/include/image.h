#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <inttypes.h>
#include <stdlib.h>

#pragma pack(push, 1)

struct pixel { uint8_t b, g, r; };

#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t const width, uint64_t const height);

void delete_image(struct image* img);

#endif
