#ifndef IMAGE_TRANSFORMER_BMP_FORMAT_H
#define IMAGE_TRANSFORMER_BMP_FORMAT_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE,
    READ_IMAGE_ERROR,
    READ_MALLOC_ERROR_CODE
};

extern char* read_status_map[];

void print_read_status(enum read_status const rs);

enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_BIT_ERROR
};

extern char* write_status_map[];

void print_write_status(enum write_status const ws);

enum write_status to_bmp( FILE* out, struct image const* img );

long get_padding(uint32_t const width);

#endif
