#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

typedef struct image (*rotate) (struct image const);

enum rotations {
        ROTATION_0,
        ROTATION_90,
        ROTATION_180,
        ROTATION_270,
        INCORRECT_ANGLE,
        INVALID_INPUT
};

enum rotations get_rotation(char const* string_angle);

extern rotate rotate_funks[];
#endif
