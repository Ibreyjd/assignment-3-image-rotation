#include "image.h"


struct image create_image(uint64_t const width, uint64_t const height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = (struct pixel*) malloc(sizeof(struct pixel) * width * height)
    };
}

void delete_image(struct image* img) {
    if (img) {
        free(img->data);
        img->data = NULL;
    }
}
