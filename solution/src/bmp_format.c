#include "bmp_format.h"

char* read_status_map[] = {
        [READ_OK] = "Picture was read successfully",
        [READ_INVALID_SIGNATURE] = "Structure of file isn't the BMP!",
        [READ_INVALID_BITS] = "BMP file isn't 24 bit!",
        [READ_INVALID_HEADER] = "Header cannot be readable!",
        [READ_INVALID_FILE] = "The name of file is NULL!",
        [READ_IMAGE_ERROR] = "Error with reading image!",
        [READ_MALLOC_ERROR_CODE] = "Img is NULL!"
};

void print_read_status(enum read_status const rs) {
    if (rs != READ_OK) {
        fprintf(stderr, "%s", read_status_map[rs]);
    } else printf( "%s", read_status_map[rs]);
}

char* write_status_map[] = {
        [WRITE_OK] = "Picture was writen successfully",
        [WRITE_BIT_ERROR] = "Error with writing pixels!",
        [WRITE_HEADER_ERROR] = "Error with writing header!"
};

void print_write_status(enum write_status const ws) {
    if (ws != WRITE_OK) {
        fprintf(stderr, "%s", write_status_map[ws]);
    } else printf("%s", write_status_map[ws]);
}

long get_padding(uint32_t const width){
    return (long) (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (!in) return READ_INVALID_FILE;
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
    if (header.bfType != 0x4d42) {
        printf("%hu", header.bfType);
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) return READ_INVALID_BITS;

    *img = create_image((uint64_t) header.biWidth,(uint64_t) header.biHeight);
    if (!img->data) return READ_MALLOC_ERROR_CODE;
    fseek(in,(long) header.bOffBits, SEEK_SET);
    long padding = get_padding(img->width);
    for(size_t i = 0; i < img->height; i++) {
        if (fread(&img->data[(img->height - i - 1) * img->width], sizeof(struct pixel), img->width, in) != img->width) return READ_IMAGE_ERROR;
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    long padding = get_padding(img->width);
    struct bmp_header header;
    header.bfType = 0x4d42;
    header.bfileSize = img->height * (img->width * sizeof(struct pixel) + padding);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (img->width + padding) * img->height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_HEADER_ERROR;
    for(size_t i = 0; i < img->height; i++) {
        if (fwrite(&img->data[(img->height - i - 1) * img->width], sizeof(struct pixel), img->width, out) != img->width) return WRITE_BIT_ERROR;
        for (size_t j = 0; j < padding; j++) {
            fputc(0, out);
        }
    }
    return WRITE_OK;
}
