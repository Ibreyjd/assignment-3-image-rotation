#include "rotate.h"
#include <stdbool.h>
#include <stdlib.h>


bool check(struct image const source, struct image const rotated_img) {
    if (!source.data || !rotated_img.data) return false;
    return true;
}

struct image rotate0( struct image const source) {
    return source;
}

struct image rotate90( struct image const source ) {
    struct image rotated_image = create_image(source.height, source.width);
    if (!check(source, rotated_image)) return (struct image) {0};
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rotated_image.data[(source.height - 1 - i) + j * source.height] = source.data[j + i * source.width];
        }
    }
    return rotated_image;
}

struct image rotate180( struct image const source ) {
    struct image rotated_image = create_image(source.width, source.height);
    if (!check(source, rotated_image)) return (struct image) {0};
    for (size_t i = 0; i < source.height; i++) {
        for(size_t j = 0; j < source.width; j++) {
            rotated_image.data[(source.height - 1 - i) * source.width + (source.width - 1 - j)] = source.data[j + i * source.width];
        }
    }
    return rotated_image;
}

struct image rotate270( struct image const source ) {
    struct image rotated_image = create_image(source.height, source.width);
    if (!check(source, rotated_image)) return (struct image) {0};
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rotated_image.data[(source.width - 1 - j) * source.height + i] = source.data[j + i * source.width];
        }
    }
    return rotated_image;
}

enum rotations get_rotation(char const* string_angle) {
    char *endptr;
    long angle = strtol(string_angle, &endptr, 10);
    if (*endptr == '\0') {
        switch (angle) {
            case 0: {return ROTATION_0;}
            case 90:case -270: {return ROTATION_90;}
            case 180: case -180: {return ROTATION_180;}
            case 270: case -90: {return ROTATION_270;}
            default: {return INCORRECT_ANGLE;}
        }
    } else return INVALID_INPUT;
}

rotate rotate_funks[] = {
        [ROTATION_0] = rotate0,
        [ROTATION_90] = rotate90,
        [ROTATION_180] = rotate180,
        [ROTATION_270] = rotate270
};
