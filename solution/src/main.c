#include "image.h"
#include "bmp_format.h"
#include "rotate.h"
#include "util.h"

int main( int argc, char** argv ) {
    FILE* f_in;
    FILE* f_out;
    enum rotations rotation = get_rotation(argv[3]);
    struct image img;
    struct image rotated_img;
    enum read_status rs;
    enum write_status ws;
    if (argc < 4) {
        fprintf(stderr, "Wrong number of input arguments\nExample: ./prog.exe file.bmp file2.bmp 90");
        return -1;
    }

    if (rotation == INCORRECT_ANGLE) {
        fprintf(stderr, "Incorrect angle, it should be (0, 90, -90, 180, -180, 270, -270)!");
        return -1;
    }
    if (rotation == INVALID_INPUT) {
        fprintf(stderr, "Invalid input, it should be int!");
        return -1;
    }

    f_in = fopen(argv[1], "rb");
    if (!f_in) {
        fprintf(stderr, "Error with opening the reading-file!");
        return -1;
    }

    rs = from_bmp(f_in, &img);
    if (fclose(f_in) == EOF) {
        fprintf(stderr, "Error with closing the file!");
        delete_image(&img);
        return -1;
    }

    print_read_status(rs);
    print_newline();

    rotated_img = rotate_funks[rotation](img);
    if (rotation != ROTATION_0) delete_image(&img);

    f_out = fopen(argv[2], "wb");
    if(!f_out) {
        fprintf(stderr, "Error with opening the writing-file!");
        delete_image(&rotated_img);
        return -1;
    }
    ws = to_bmp(f_out, &rotated_img);
    delete_image(&rotated_img);
    print_write_status(ws);

    return 0;
}
